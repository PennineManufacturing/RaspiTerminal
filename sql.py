import pymssql
import datetime


class SqlTerminal:
    con = False

    def __init__(self):
        self.con = pymssql.connect('192.168.0.4', 'Sa', 'Aeqpzmuc1', 'MachineMonitor')
        self.mmm2 = pymssql.connect('192.168.0.4', 'Sa', 'Aeqpzmuc1', 'myMachineMonitor2')

    def getLastRecordNumber(self, controllerID):
        cursor = self.mmm2.cursor()
        cursor.execute("SELECT TOP (1) [ReaderIndex] FROM [myMachineMonitor2].[dbo].[ReaderData] where controllerID = {0} order by ReaderIndex desc".format(controllerID))
        return int(cursor.fetchone()[0]) + 1


    def getTerminalID(self, ip):
        cursor = self.con.cursor()
        cursor.execute("""
        SELECT [id]
      ,[hostname]
      ,[mac_address]
      ,[ip_address]
      ,[created_at]
      ,[updated_at]
      ,[deleted_at]
      FROM [MachineMonitor].[dbo].[terminals]
      where ip_address = '%s'
        """ % str(ip))
        return cursor.fetchone()

    def insertMonitorRecord(self, id, type, data, time):
        cursor = self.con.cursor()
        cursor.execute(
            "INSERT INTO monitorrecord (record_type, record_data, terminal_id, created_at)"
            "values('{0}', '{1}', '{2}', '{3}')".format(type, data, id, time)
        )
        self.con.commit()

    def insertMonitorRecordMMM2(self, EventType, controllerID):
        now = datetime.datetime.now()
        dtReading = str(now.strftime("%Y-%m-%d %H:%M:%S"))
        ReaderDate = str(now.strftime("%d/%m/%Y"))
        ReaderTime = str(now.strftime("%H:%M"))

        cursor = self.mmm2.cursor()
        cursor.execute(
        "INSERT INTO ReaderData (Readerindex,CardID ,ReaderDate, ReaderTime, EventType,controllerID, dtReading, bProcessed)"
        "values('{0}', '{1}', '{2}', '{3}', '{4}','{5}','{6}','{7}')".format(self.getLastRecordNumber(controllerID),'FFFFFFF0', ReaderDate, ReaderTime, EventType, int(controllerID), dtReading,0)
        )
        self.mmm2.commit()

    def getCardType(self, card_id):
        cursor = self.con.cursor()
        cursor.execute(
            "SELECT card_type FROM employeecards WHERE(card_id='{}')".format(card_id)
        )
        tables = cursor.fetchone()
        return tables


    def getMMTerminalID(self,terminalID):
            cursor = self.con.cursor()
            cursor.execute("""
            SELECT terminalMap.mmMap
            FROM            [terminals] INNER JOIN
                         [terminalMap] ON terminals.id = terminalMap.terminal_id
            where terminals.id = '%s'
            """ % str(terminalID))
            return cursor.fetchone()[0]