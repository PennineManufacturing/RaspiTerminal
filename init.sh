#!/usr/bin/env bash

init(){

    Welcome

    setup_terminal
    clone_repo

    sudo reboot
}


Welcome(){

    echo " _____                 _              __  __            _     _              __  __             _ _             "
    echo "|  __ \               (_)            |  \/  |          | |   (_)            |  \/  |           (_) |            "
    echo "| |__) |__ _ __  _ __  _ _ __   ___  | \  / | __ _  ___| |__  _ _ __   ___  | \  / | ___  _ __  _| |_ ___  _ __ "
    echo "|  ___/ _ \ '_ \| '_ \| | '_ \ / _ \ | |\/| |/ _\` |/ __| \'_ \| | \'_ \ / _ \ | |\/| |/ _ \| \'_ \| | __/ _ \| \'__|"
    echo "| |  |  __/ | | | | | | | | | |  __/ | |  | | (_| | (__| | | | | | | |  __/ | |  | | (_) | | | | | || (_) | |   "
    echo "|_|   \___|_| |_|_| |_|_|_| |_|\___| |_|  |_|\__,_|\___|_| |_|_|_| |_|\___| |_|  |_|\___/|_| |_|_|\__\___/|_|   "
    echo ""

    echo "This program will run you though installing the terminal"
    echo
    echo
}


set_ssh(){
    echo "What password would you like to use to connect remotely to this device?"
    read password
    sudo passwd $password
    echo "Your SSH password is now" $password
    echo
    echo "Connect to this device from windows with ssh pi@device ip and then type the password" $password
    echo

    install_dependencies



}


install_dependencies(){
    sudo apt-get update -y
    sudo apt-get upgrade -y
    sudo apt-get install -y chromium-browser ttf-mscorefonts-installer unclutter x11-xserver-utils freetds-dev

    clone_repo

}


clone_repo(){
    sudo git clone https://penninemanufacturing:Aeqpzmuc1@github.com/oisian/RaspiTerminal
    setup_terminal
}

setup_terminal(){
    sudo echo @/usr/bin/python /home/pi/RaspiTerminal/main.py
    sudo echo @/usr/bin/chromium-browser --force-device-scale-factor=0.79 --incognito --start-maximized --kiosk http://mm.internal.penninemanufacturing.co.uk/terminal
    sudo echo @lxpanel --profile LXDE-pi
    sudo echo @pcmanfm --desktop --profile LXDE-pi
    sudo echo @point-rpi
    sudo echo @unclutter
    sudo echo @xset s off
    sudo echo @xset s noblank
    sudo echo @xset -dpms

}

init