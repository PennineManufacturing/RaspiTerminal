import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
machine_shot_pin = 2
machine_scrap_pin = 3
machine_heat_pin = 23
machine_pump_pin = 17
machine_set_scrap = 22



GPIO.setup(machine_shot_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # machine_shot
GPIO.setup(machine_scrap_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # machine_scrap
GPIO.setup(machine_heat_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # machine_heat
GPIO.setup(machine_pump_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # machine_pump
GPIO.setup(machine_set_scrap, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # machine_pump

while 1:
        print("SHOT {}".format(str(GPIO.input(machine_shot_pin))))
        print("Scrap {}".format(not GPIO.input(machine_scrap_pin)))
        print("Setter Scrap {}".format(not GPIO.input(machine_set_scrap)))
        print("Heat {}".format(not GPIO.input(machine_heat_pin)))
        print("Pump {}".format(not GPIO.input(machine_pump_pin)))
        print("---------")
        time.sleep(0.1)