import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(19, GPIO.OUT)
GPIO.setup(15, GPIO.IN)
p = GPIO.PWM(19, 1)
p.start(100)

while True:
    p.ChangeDutyCycle(100)
    print(GPIO.input(15))


GPIO.cleanup()