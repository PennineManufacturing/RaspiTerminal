from gpiozero import Button
import RPi.GPIO as GPIO
import threading
import sql
import datetime
import time
import Rfid
import re
import sys, os

class Terminal:
    GPIO.setmode(GPIO.BCM)

    terminal_id = False

    sql_queue = []
    sql_queueMM2 = []
    uncommitted_record = []

    machine_shot_pin = 2
    machine_scrap_pin = 3
    machine_heat_pin = 27
    machine_pump_pin = 17
    machine_set_scrap = 22


    GPIO.setup(machine_shot_pin, GPIO.IN , pull_up_down=GPIO.PUD_UP)  # machine_shot
    GPIO.setup(machine_scrap_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # machine_scrap
    GPIO.setup(machine_heat_pin, GPIO.IN , pull_up_down=GPIO.PUD_UP)  # machine_heat
    GPIO.setup(machine_pump_pin, GPIO.IN , pull_up_down=GPIO.PUD_UP)  # machine_pump
    GPIO.setup(machine_set_scrap, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # machine_pump

    shot_state = 0
    heat_state = 0
    scrap_state = 0
    set_scrap_state = 0
    pump_state = 0
    rfid = False
    sql = False

    def __init__(self):
        self.sql = sql.SqlTerminal()
        ipadd = ""
        if (os.popen('ip addr show eth0 | grep "\<inet\>" | awk \'{ print $2 }\' | awk -F "/" \'{ print $1 }\'').read().strip()):
            ipadd = os.popen('ip addr show eth0 | grep "\<inet\>" | awk \'{ print $2 }\' | awk -F "/" \'{ print $1 }\'').read().strip()
        elif (os.popen('ip addr show wlan0 | grep "\<inet\>" | awk \'{ print $2 }\' | awk -F "/" \'{ print $1 }\'').read().strip()):
            ipadd = os.popen('ip addr show wlan0 | grep "\<inet\>" | awk \'{ print $2 }\' | awk -F "/" \'{ print $1 }\'').read().strip()

        self.terminal_id = self.sql.getTerminalID(ipadd)[0]
        self.mmm_terminal_id = self.sql.getMMTerminalID(self.terminal_id)
        self.init_pump_heat()
        print("Starting")

    def init_pump_heat(self):
        self.pump_state =  not GPIO.input(self.machine_pump_pin)
        self.heat_state =  not GPIO.input(self.machine_heat_pin)
        self.scrap_state =   GPIO.input(self.machine_scrap_pin)
        self.shot_state =   GPIO.input(self.machine_shot_pin)
        self.set_scrap_state = GPIO.input(self.machine_set_scrap)

        self.sql_queue.append([self.terminal_id, "Pump", int(not GPIO.input(self.machine_pump_pin)), str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))])
        self.sql_queue.append([self.terminal_id, "Heat", int(not GPIO.input(self.machine_heat_pin)), str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))])

    def shot_detection(self, *args):
        if GPIO.input(self.machine_shot_pin) != self.shot_state:
            self.shot_state = GPIO.input(self.machine_shot_pin)
            if self.shot_state:
                self.sql_queue.append([self.terminal_id, "Shot", 1, str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))])
                self.sql_queueMM2.append(["(0x03)Door state low", self.mmm_terminal_id])
                return True
            return False
        else:
            return False

    def scrap_detection(self, *args):
        if GPIO.input(self.machine_scrap_pin) != self.scrap_state:
            self.scrap_state = GPIO.input(self.machine_scrap_pin)
            if self.scrap_state:
                self.sql_queue.append([self.terminal_id, "Scrap", 1, str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))])
                self.sql_queueMM2.append(["Scrap", self.mmm_terminal_id])
                return True
            return False
        else:
            return False


    def set_scrap_detection(self, *args):
        if GPIO.input(self.machine_set_scrap) != self.set_scrap_state:
            self.set_scrap_state = GPIO.input(self.machine_set_scrap)
            if self.set_scrap_state:
                self.sql_queue.append([self.terminal_id, "Setter Scrap", 1, str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))])
                self.sql_queueMM2.append(["Setter Scrap", self.mmm_terminal_id])
                return True
            return False
        else:
            return False

#todo, this only works for old machine monitor, as that relys on a fall off time, this wont send a record saying heats are off on state change
    def heat_detection(self, *args):
        if  not GPIO.input(self.machine_heat_pin):
            self.heat_state =  not GPIO.input(self.machine_heat_pin)
            self.sql_queue.append([self.terminal_id, "Heat", int(self.heat_state), str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))])
            self.sql_queueMM2.append(["(0x05)Open switch", self.mmm_terminal_id])
            return True
        else:
            return False

    def pump_detection(self, *args):
        if  (not GPIO.input(self.machine_pump_pin))  != self.pump_state:
            self.pump_state = not GPIO.input(self.machine_pump_pin)
            self.sql_queue.append([self.terminal_id, "Pump", int(self.pump_state), str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))])
            self.sql_queueMM2.append(["pump", self.mmm_terminal_id])
            return True
        else:
            return False


    def file_process(self):
        while 1:
            time.sleep(25)
            if len(self.uncommitted_record) > 0:
                print(self.uncommitted_record)
                with open('records.txt', 'a+') as file:
                    for record in list(terminal.uncommitted_record):
                        file.write(str(record[0]) + "," + str(record[1]) + "\n")
                        self.uncommitted_record.remove(record)

            lines = [line.rstrip() for line in open('records.txt', 'w+')]
            for line in lines:
                split = line.split(',')
                try:
                    self.sql.insertMonitorRecordMMM2(split[0], split[1])
                except:
                    print("still unable to post record to DB {0}, {1}".format(split[0], split[1]))
            open('records.txt', 'w+').close()


    def swipe_card_detection(self):
        while 1:
            card_id = self.rfid.pollForSwipeCard()
            card_type = self.sql.getCardType(card_id)
            self.rfid.swipe = ""
            print(card_id)
            if card_type:
                card_type = re.findall(r"'(.*?)'", str(card_type))

                if card_type[0] == "Scrap":
                    self.sql_queue.append([self.terminal_id, card_type[0], card_id,
                                           str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))])
                    time.sleep(5)
                elif card_type[0] == "First Off":
                    self.sql_queue.append([self.terminal_id, card_type[0], card_id,
                                           str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))])
                    time.sleep(5)
                elif card_type[0] == "QC Pass":
                    self.sql_queue.append([self.terminal_id, card_type[0], card_id,
                                           str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))])
                    time.sleep(5)
                elif card_type[0] == "QC Fail":
                    self.sql_queue.append([self.terminal_id, card_type[0], card_id,
                                       str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))])
                    time.sleep(5)
                else:
                    time.sleep(5)
            else:
                time.sleep(5)

    def get_shot_state(self):
        return self.shot_state

    def get_heat_state(self):
        return self.heat_state

    def get_scrap_state(self):
        return self.scrap_state

    def get_pump_state(self):
        return self.pump_state


    def execute_query(self):
        while 1:
            time.sleep(1)
            if len(self.sql_queue) > 0 :
                for record in list(self.sql_queue):
                    try:
                        self.sql.insertMonitorRecord(record[0],record[1],record[2],record[3])
                        self.sql_queue.remove(record)
                    except:
                        print("Failed to insert record in to MachineMonitor, writing to disk")
                        self.uncommitted_record.append((record[0],record[1],record[2],record[3]))
            time.sleep(1)
            if len(self.sql_queueMM2) > 0 :
                for record in list(self.sql_queueMM2):
                    try:
                        self.sql.insertMonitorRecordMMM2(record[0], record[1])
                        self.sql_queueMM2.remove(record)
                    except:
                        print("Oops!", sys.exc_info()[0], "occured.")
                        print("Failed to insert record in to mmm2, writing to disk")

    def poll_shot(self):
        while 1:
            if self.shot_detection():
                print("shot detection")
            time.sleep(0.1)

    def poll_scrap(self):
        while 1:
            if self.scrap_detection():
                print("Scrap detection")
            time.sleep(0.1)

    def poll_set_scrap(self):
        while 1:
            if self.set_scrap_detection():
                print("setter Scrap detection")
            time.sleep(0.1)

    def poll_heat(self):
        while 1 :
            time.sleep(120)
            if self.heat_detection():
                if self.get_heat_state():
                    print("Heats on")
                else:
                    print("Heats off")
            time.sleep(0.1)

    def poll_pump(self):
        while 1:
            if self.pump_detection():
                if self.get_pump_state():
                     print("Pumps on")
                else:
                     print("Pumps off")
            time.sleep(0.1)

    def run(self):

        query_thread = threading.Thread(target=self.execute_query)
        file_thread = threading.Thread(target=self.file_process)
        shot_thread = threading.Thread(target=self.poll_shot)
        scrap_thread = threading.Thread(target=self.poll_scrap)
        set_scrap_thread = threading.Thread(target=self.poll_set_scrap)
        pump_thread = threading.Thread(target=self.poll_pump)
        heat_thread = threading.Thread(target=self.poll_heat)


        scrap_thread.start()
        set_scrap_thread.start()
        pump_thread.start()
        heat_thread.start()
        shot_thread.start()
        file_thread.start()
        query_thread.start()


terminal = Terminal()
terminal.run()

